# opencpi.gitlab.io

This project hosts the files that make up https://opencpi.gitlab.io. A lot of those files are created and deployed when
the CI/CD pipeline runs.

## Built Files

What is built:
- [OpenCPI](https://gitlab.com/opencpi/opencpi) docs (pdfs) for every tagged release of OpenCPI
  - deployed to https://opencpi.gitlab.io/releases/

## Static Content

RPMS:
- Yum repo files are located under `public/repo/`
- RPMS are hosted on AWS S3
- A "mirror list" file is used to tell yum where to look for repo metadata

To enable access to the OpenCPI RPM repo download the .repo file for the
version of OpenCPI you want to install using RPMS and place it in `/etc/yum.repos.d/`

**Latest version example:**
```bash
sudo rm -f /etc/yum.repos.d/opencpi*.repo
sudo curl https://opencpi.gitlab.io/repo/opencpi.repo > /etc/yum.repos.d/
sudo yum install epel-release
sudo yum install opencpi opencpi-devel opencpi-driver
```

To see all available OpenCPI RPMS:
```bash
sudo yum list 'opencpi*'
```
