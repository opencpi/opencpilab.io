[//]: # (These are reference links used in the body of this document and get
         stripped out when the markdown processor does its job. The blank lines
         before and after these reference links are important for portability
         across different markdown parsers.)

[groupurl]: <https://gitlab.com/opencpi>

[opencpiurl]: <https://gitlab.com/opencpi/opencpi>

[contributing]: <https://gitlab.com/opencpi/opencpi/blob/develop/CONTRIBUTING.md>

[issuelist]:  <https://gitlab.com/opencpi/opencpi.gitlab.io/issues>


# Contributing to opencpi.gitlab.io
First off, thanks for taking the time to contribute and reading this document!

The following is a set of guidelines for contributing to opencpi.gitlab.io. These
are mostly guidelines, not rules. Use your best judgement and feel free to
propose changes to this document in a merge request.

**All contributions become part of OpenCPI, subject to the LGPL3 license,
unless other arrangements are specifically made (which is rare).**

# What to Know Before Getting Started
This project is part of the [OpenCPI framework][groupurl] and is used to extend
or add additional functionality to the core of [OpenCPI][opencpiurl]. You
should read [OpenCPI's Contribution Guide][contributing] as the same guidelines
apply to this project as well.

Before investing time on a contribution it is a good idea to find out if anyone
is already working on the same or similiar thing by using this project's
[issue list][issuelist]. Another source of information is the email discussion
list at discuss@lists.opencpi.org. To post on this list you must subscribe to it
at http://lists.opencpi.org.
